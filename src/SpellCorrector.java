
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.text.DecimalFormat;

public class SpellCorrector {

    final private CorpusReader cr;
    final private ConfusionMatrixReader cmr;
    final double ScalerLow = Math.pow(10, 2);
    DecimalFormat fmt = new DecimalFormat("#.####");

    final char[] ALPHABET = "abcdefghijklmnopqrstuvwxyz'".toCharArray();

    public SpellCorrector(CorpusReader cr, ConfusionMatrixReader cmr) {
        this.cr = cr;
        this.cmr = cmr;
    }

    public String correctPhrase(String phrase) {
        if (phrase == null || phrase.length() == 0) {
            throw new IllegalArgumentException("phrase must be non-empty.");
        }

        String[] words = phrase.split(" ");
        //String[] wordsOriginal = words.clone();
        String finalSuggestion = "";

        // 1st pass (loking = looking, for a car)
        ArrayList<Integer> nonVocWordIds = replaceInvalidWords(words);

        // determine which words can still be changed
        boolean[] changable = changableNess(nonVocWordIds, words.length);

        // do the 2nd pass on only correct (booking = looking, for a car)
        if (nonVocWordIds.size() < 3) {
            improoveSmoothness(words, changable, 3 - nonVocWordIds.size());
        }

        finalSuggestion = toString(words);

        return finalSuggestion.trim();
    }

    private String toString(String[] s) {
        String result = s[0];
        for (int i = 1; i < s.length; i++) {
            result += " " + s[i];
        }
        return result;
    }

    public double calculateChannelModelProbability(String suggested, String incorrect) {
        /**
         * CODE TO BE ADDED *
         */

        int vocSize = cr.getVocabularySize();
        int wordFrequency = cr.getNGramCount(suggested);
        double pWord = wordFrequency / (double) (vocSize);

        char[] sug = suggested.toCharArray();
        char[] inc = incorrect.toCharArray();

        int confSize = cmr.matrixCount; //cmr.getConfusionSize();
        int errorCount = 0;
        double pXW = 0;

        boolean found = false;

        if (suggested.equals(incorrect)) {
            //HashSet<String> canidates = getCandidateWords(suggested);
            pXW = 0.95;
        } else if (sug.length == inc.length) {
            //transposition or substitution
            int i = 0;
            for (; i < sug.length; i++) {
                if (sug[i] != inc[i]) {
                    break; //break where not the same
                }
            }
            //i is the place in chararray where the difference is

            //substitution
            if (sug.length == 1) {
                errorCount = cmr.getConfusionCount(sug[i] + "", inc[i] + "");
                found = true;
            }
            else if (i < sug.length - 1 && i > 0 && !found) {
                if (sug[i + 1] == inc[i + 1] && sug[i - 1] == inc[i - 1]) {
                    errorCount = cmr.getConfusionCount(sug[i] + "", inc[i] + "");
                    found = true;
                }
            } else if (i == 0 && !found) {
                if (sug[i + 1] == inc[i + 1]) {
                    errorCount = cmr.getConfusionCount(sug[i] + "", inc[i] + "");
                    //System.out.println("as");
                    found = true;
                }
            } else if (i == sug.length - 1 && !found) {
                if (sug[i - 1] == inc[i - 1]) {
                    errorCount = cmr.getConfusionCount(sug[i] + "", inc[i] + "");
                    //System.out.println("as2");
                    found = true;
                }
            }

            //transposition
            if (i < sug.length - 1 && !found) {
                if (sug[i] == inc[i + 1] && sug[i + 1] == inc[i]) {
                    char[] error = new char[]{sug[i + 1], sug[i]};
                    char[] correct = new char[]{sug[i], sug[i + 1]};
                    errorCount = cmr.getConfusionCount(new String(error), new String(correct));
                    found = true;
                }
            }
        } else if (sug.length == inc.length + 1 && !found) { //inclusion
            int i = 0;
            for (; i < inc.length; i++) {
                if (sug[i] != inc[i]) {
                    break; //break where not the same
                }
            }
            //i is the place in chararray where the difference is

            if (i == 0) {
                errorCount = cmr.getConfusionCount(" ", sug[i] + ""); //space becomes char
                found = true;
                //System.out.println("count: " + errorCount);
            } else {
                char[] error = new char[]{inc[i - 1]};
                char[] correct = new char[]{sug[i - 1], sug[i]};
                errorCount = cmr.getConfusionCount(new String(error), new String(correct));
                found = true;
                //System.out.println("count: " + errorCount + new String(error) + " " + new String(correct));
            }

        } else if (sug.length == inc.length - 1 && !found) { //deletion
            int i = 0;
            for (; i < inc.length - 1; i++) {
                if (sug[i] != inc[i]) {
                    break; //break where not the same
                }
            }
            //i is the place in chararray where the difference is

            if (i == 0) {
                errorCount = cmr.getConfusionCount(inc[i] + "", " "); //space becomes char
                found = true;
            } else {
                char[] error = new char[]{inc[i - 1], inc[i]};
                char[] correct = new char[]{sug[i - 1]};
                errorCount = cmr.getConfusionCount(new String(error), new String(correct));
                found = true;
            }

        }
        if (found) {
            pXW = (errorCount / (double) confSize);
        }

        return ScalerLow * pXW * pWord;
    }

    /*
     returns the id's of replaced words
     */
    private ArrayList<Integer> replaceInvalidWords(String[] words) {
        /*
         Replace all words that are not in the vocabulary
         */

        boolean print = false;
        ArrayList<Integer> nonVocWordIds = not_in_voc(words);
        if (print) {
            System.out.println("Not in vocabulary : " + nonVocWordIds.toString());
        }

        // for all wrong words
        for (Integer nonVocWordId : nonVocWordIds) {

            // get replacements
            HashSet<String> candidates = getCandidateWords(words[nonVocWordId]);

            if (print) {
                System.out.println("Canidates for word \"" + words[nonVocWordId] + "\" : " + candidates.toString());
            }

            double highest_probability = Double.MIN_VALUE;
            // for each replacement
            for (String candidate : candidates) {

                // determine replacement probabilty
                double candidate_typo_chance = 1 + calculateChannelModelProbability(candidate, words[nonVocWordId]);
                double candidate_smooth_value = cr.getSmoothedCount(makeNewTriMaxSentence(words, nonVocWordId, candidate));

                double probability = candidate_typo_chance * candidate_smooth_value;

                if (print) {
                    System.out.print("\t candidate [" + candidate + "] \ttypo value:   " + fmt.format(candidate_typo_chance));
                }
                if (print) {
                    System.out.println("  \tsmooth value: " + fmt.format(candidate_smooth_value));
                }

                // update to the highest probability
                if (probability > highest_probability) {
                    if (print) {
                        System.out.println("\tREPLACE : " + words[nonVocWordId] + " by " + candidate + "\t@ value : " + probability);
                    }
                    highest_probability = probability;
                    words[nonVocWordId] = candidate;
                }
            }
        }

        return nonVocWordIds;
    }

    /*
     improoves the relatedness 
     */
    private void improoveSmoothness(String[] words, boolean[] changable, int changesLeft) {

        //System.out.println("\nImproove smoothness of : \n\t" + toString(words));
        /*
         Determine (for each changeble word) how much we want to change it
         */
        double[] baseValue = new double[words.length];
        String[] AlternativeStrings = new String[words.length];
        double[] AlternativeValues = new double[words.length];

        // for each word
        for (int w = 0; w < words.length; w++) {
            // that is changable
            if (changable[w]) {
                // determine current value
                double ccmp = 1 + calculateChannelModelProbability(words[w], words[w]);
                baseValue[w] = cr.getSmoothedCount(makeNewTriMaxSentence(words, w)) * ccmp;

                // get alternatives
                HashSet<String> data = getCandidateWords(words[w]);
                String[] alternatives = data.toArray(new String[data.size()]);

                AlternativeValues[w] = Double.MIN_VALUE;
                
                //System.out.println("Considering : " + words[w] + " \t baseValue : " + baseValue[w] );

                // and determine best altrnative
                for (int i = 0; i < alternatives.length; i++) {
                    //double thisLoose = 1 + cr.getNGramCount(alternatives[i]);
                    
                    double ccmpA = 1 + calculateChannelModelProbability(alternatives[i], words[w]);
                    double thisVal = /*thisLoose * */ cr.getSmoothedCount(makeNewTriMaxSentence(words, w, alternatives[i])) * ccmpA;
                   
                    if (words[w].equals("winnings")) {
                        System.out.println("Considering : " + words[w] + " \t baseValue : " + baseValue[w] );
                        System.out.println("\twith string : " + alternatives[i] + "\tVal : " + thisVal);
                    }
                    
                    if (thisVal > AlternativeValues[w]) {
                        AlternativeValues[w] = thisVal;
                        AlternativeStrings[w] = alternatives[i];
                    }
                }

                //System.out.println("\tBestAlternativeValue : " + AlternativeValues[w] + " with string : " + AlternativeStrings[w]);
            }
        }

        double[] changeQuality = new double[words.length];

        // for each word
        for (int w = 0; w < words.length; w++) {
            // if changble
            if (changable[w]) {
                // set the change quality of this word
                changeQuality[w] = AlternativeValues[w] / baseValue[w];
            }
        }

        // change the words in order of best to worst, given a minimal quality and maximum of changes
        while (changesLeft > 0) {

            int bestChangeWord = 0;
            double bestChangeQuality = Double.MIN_VALUE;

            // for each word
            for (int w = 0; w < words.length; w++) {
                // if changble
                if (changable[w]) {
                    // and its quality is better than the best
                    if (changeQuality[w] > bestChangeQuality) {
                        // replace the best by this one
                        bestChangeQuality = changeQuality[w];
                        bestChangeWord = w;
                    }
                }
            }

            if (bestChangeQuality > 1 && changeQuality[bestChangeWord] > 10) {
                // update the word
                words[bestChangeWord] = AlternativeStrings[bestChangeWord];
                setChanged(changable, bestChangeWord);
                changesLeft -= 1;
            } else {
                break;
            }
        }
    }
    
    private boolean[] changableNess(ArrayList<Integer> changedWordIds, int wordCount) {
        //set unchangable words       
        boolean[] changable = new boolean[wordCount];
        Arrays.fill(changable, true);
        for (Integer nonVocWordId : changedWordIds) {
            setChanged(changable, nonVocWordId);
        }

        return changable;
    }

    private void setChanged(boolean[] changable, int id) {

        if (changable[id] == false) {
            System.out.println("<< ERROR, UNCHANGABLE CHANGED >>");
        }

        if (id > 0) {
            changable[id - 1] = false;
        }

        changable[id] = false;

        if (id + 1 != changable.length) {
            changable[id + 1] = false;
        }
    }

    private String makeNewTriMaxSentence(String[] words, int loc, String replaceBy) {
        String result = "";
        for (int i = Math.max(0, loc - 1); i < Math.min(words.length, loc + 2); i++) {
            if (i != loc) {
                result += " " + words[i];
            } else {
                result += " " + replaceBy;
            }
        }
        return result.trim();
    }

    private String makeNewTriMaxSentence(String[] words, int loc) {
        String result = "";
        for (int i = Math.max(0, loc - 1); i < Math.min(words.length, loc + 2); i++) {
            result += " " + words[i];
        }
        return result.trim();
    }

    /*
     Made by bart
     */
    private ArrayList<Integer> not_in_voc(String[] sentence) {
        ArrayList<Integer> wrong_words = new ArrayList<Integer>();

        for (int i = 0; i < sentence.length; i++) {
            if (!cr.inVocabulary(sentence[i])) {
                wrong_words.add(i);
            }
        }

        return wrong_words;
    }

    public HashSet<String> getCandidateWords(String word) {
        HashSet<String> ListOfWords = new HashSet<String>();

        /**
         * CODE TO BE ADDED *
         */
        char[] characters = word.toCharArray();

        for (int i = 0; i < characters.length; i++) {
            //substitution
            for (int j = 0; j < ALPHABET.length; j++) {
                char[] changedWord = word.toCharArray();
                changedWord[i] = ALPHABET[j];
                ListOfWords.add(new String(changedWord));
            }

            //deletion
            char[] changedWord = new char[word.length() - 1];
            for (int j = 0; j < i; j++) {
                changedWord[j] = characters[j];
            }
            for (int j = i + 1; j < characters.length; j++) {
                changedWord[j - 1] = characters[j];
            }
            ListOfWords.add(new String(changedWord));

            //insertion (before each character in loop)
            for (int j = 0; j < ALPHABET.length; j++) {
                changedWord = new char[word.length() + 1];
                for (int k = 0; k < i; k++) { //before insertion
                    changedWord[k] = characters[k];
                }
                changedWord[i] = ALPHABET[j]; //inserting
                for (int k = i + 1; k < characters.length + 1; k++) { //after insertion
                    changedWord[k] = characters[k - 1];
                }
                ListOfWords.add(new String(changedWord));
            }

            //transposition
            changedWord = new char[word.length()];
            for (int j = 0; j < word.length(); j++) {
                changedWord[j] = characters[j];
            }
            if (i < word.length() - 1) {
                char temp = changedWord[i];
                changedWord[i] = changedWord[i + 1];
                changedWord[i + 1] = temp;
            }
            //System.out.println(new String(changedWord));
            ListOfWords.add(new String(changedWord));
        }

        //insertion (after the last character)
        for (int j = 0; j < ALPHABET.length; j++) {
            char[] changedWord = new char[word.length() + 1];
            for (int k = 0; k < word.length(); k++) { //before insertion
                changedWord[k] = characters[k];
            }
            changedWord[word.length()] = ALPHABET[j]; //inserting
            ListOfWords.add(new String(changedWord));
        }

        ListOfWords = cr.inVocabulary(ListOfWords);
        ListOfWords.remove(word);
        return ListOfWords;
    }
}
