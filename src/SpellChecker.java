
import java.io.IOException;
import java.util.Scanner;

public class SpellChecker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean inPeach = false; // set this to true if you submit to peach!!!

        try {
            CorpusReader cr = new CorpusReader();
            ConfusionMatrixReader cmr = new ConfusionMatrixReader();
            SpellCorrector sc = new SpellCorrector(cr, cmr);
            if (inPeach) {
                peachTest(sc);
            } else {
                nonPeachAutoTest(sc);
            }
        } catch (Exception ex) {
            System.out.println(ex);
            ex.printStackTrace();
        }
    }

    static void nonPeachTest(SpellCorrector sc) throws IOException {
        String[] sentences = {
            //"this assay allowed us to measure a wide variety of conditions",
            "this assay allowed us to measure a wide variety of conitions",
            "this assay allowed us to meassure a wide variety of conditions",
            "this assay allowed us to measure a wide vareity of conditions",
            //"at the home locations there were traces of water",
            "at the hme locations there were traces of water",
            "at the hoome locations there were traces of water",
            "at the home locasions there were traces of water",
            //"the development of diabetes is present in mice that carry a transgen",
            "the development of diabetes is present in moce that carry a transgen",
            "the development of idabetes is present in mice that carry a transgen",
            "the development of diabetes us present in mice that harry a transgen"

        };

        for (String s0 : sentences) {
            //System.out.println("Input : \t" + s0);
            String result = sc.correctPhrase(s0);
            System.out.println("Input : \t" + s0);

            System.out.println("Answer: \t" + result);
            System.out.println();
        }
    }

    static void nonPeachAutoTest(SpellCorrector sc) {
        String[][] sentences = {
            { "i have an astonishing", 
            "i have an astonishing" },
            
            // first string is the correct one, the folloing wrong versions
            {"this assay allowed us to measure a wide variety of conditions",
                "this assay allowed us to measure a wide variety of conitions",
                "this assay allowed us to meassure a wide variety of conditions",
                "this assay allowed us to measure a wide vareity of conditions"},
            {"at the home locations there were traces of water",
                "at the hme locations there were traces of water",
                "at the hoome locations there were traces of water",
                "at the home locasions there were traces of water"},
            {"the development of diabetes is present in mice that carry a transgene",
                "the development of diabetes is present in moce that carry a transgen",
                "the development of idabetes is present in mice that carry a transgen",
                "the development of diabetes us present in mice that harry a transgen"},
            {"we are looking for a new car",
                "we are booking for a new car",
                "we are booking for a new caar",
                "me are booking for e new car",
                "me are looking for e new cars",
                "we are looking for an new cart",
                "we art looking fork a now car",},
            {"i will buy a new boat",
                "i will buy i new boat",
                "a will buy i new boat",}, 
        };

        boolean allCorrect = true;
        for (int i = 0; i < sentences.length; i++) {
            for (int j = 1; j < sentences[i].length; j++) {

                String result = sc.correctPhrase(sentences[i][j]);

                if (!result.equals(sentences[i][0])) {
                    allCorrect = false;
                    System.out.println("Incorrect : ");

                    System.out.println("\t INPUT:     " + sentences[i][j]);
                    System.out.println("\t OUTPUT:    " + result);
                    System.out.println("\t COCRRECT:  " + sentences[i][0]);
                }
            }
        }
        if (allCorrect) {
            System.out.println("<< All sentences OK >>");
        }
    }

    static void peachTest(SpellCorrector sc) throws IOException {
        Scanner input = new Scanner(System.in);

        String sentence = input.nextLine();
        System.out.println("Answer: " + sc.correctPhrase(sentence));
    }
}
